// eslint-disable-next-line no-unused-vars
const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password to be true', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has not alphabet in password to be false', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})
describe('Test Digit', () => {
  test('should has digit inpassword', () => {
    expect(checkDigit('mmm1kd')).toBe(true)
  })
})
describe('Test Symbol', () => {
  test('should has Symbol ! inpassword', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should has Symbol ~ inpassword', () => {
    expect(checkSymbol('18~11')).toBe(true)
  })
})
describe('Test Password', () => {
  test('should password mild.2543 to be true', () => {
    expect(checkPassword('mild.2543')).toBe(true)
  })
  test('should password Mmild3 to be false', () => {
    expect(checkPassword('Mmild3')).toBe(false)
  })
  test('should password Mmildttm. to be false', () => {
    expect(checkPassword('Mmildttm.')).toBe(false)
  })
  test('should password mmild235 to be false', () => {
    expect(checkPassword('mmild235')).toBe(false)
  })
})
